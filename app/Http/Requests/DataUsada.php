<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use App\Reserva;
use App\Reserva_item;
use App\Item;

class DataUsada extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(){

        $input= Request::all();

        $ini=$input['inicio'];
        $fim=$input['fim'];
        $item=$input['itens'];
        
        //$agendamentos=Reserva::where('item',$item)->get();
        
        $agendamentos= Reserva_item::where('id_item',$item)->get();

        if($agendamentos!= null && sizeof($agendamentos)!=0){
            for($i=0;$i<sizeof($agendamentos);$i++){
                $idRes[]=$agendamentos[$i]->id_reserva;
            }
            $idRes=array_unique($idRes);

            for($i=0;$i<sizeof($idRes);$i++){
                $reservas[]=Reserva::where('id',$idRes[$i])->first();
            }
            for($i=0;$i<sizeof($reservas);$i++){
                if($ini>=$reservas[$i]->retirada && $ini<=$reservas[$i]->devolucao || 
                    $fim>=$reservas[$i]->retirada && $fim<=$reservas[$i]->devolucao){
                        return [$reservas[$i]->quantidade];
                }
            }
        }
        return (["Agendamentos: ".$agendamentos]);
    }
    
}