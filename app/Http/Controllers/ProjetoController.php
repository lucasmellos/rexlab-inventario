<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Projeto;
use Route;
use Request;
class ProjetoController extends Controller {
    
        protected function criar_projeto(){
            
            return view('projeto.adicionaProjeto');

        }

        protected function crie_projeto(){
            $input = Request::all();
            
            $new['nome'] = $input['nome'];
            $new['agencia'] = $input['agencia'];
            $new['inicio'] = $input['inicio'];
            $new['fim'] = $input['fim'];
            
            Projeto::create($new);
            return redirect('/projeto/todos-projetos');        
        }
        
        public function mostrar_todos(){
        //Atribuindo items toda a tabela e retornando para view ------------------------|
            $projetos = Projeto::all();
            
        return View('projeto.todosProjetos', compact('projetos'));
    }

        protected function editar_projeto(){
            $id = Route::getCurrentRoute()->parameters()['id'];
            $projeto = Projeto::where('id','=',$id)->first();
       
            return view('projeto.editarProjeto', compact('projeto'));
        }
        
        protected function edite_projeto(){
            $input = Request::all();
            
            $id = Route::getCurrentRoute()->parameters()['id'];
            $projeto = Projeto::where('id','=',$id)->first();
            
            $projeto->nome = $input["nome"];
            $projeto->agencia  = $input["agencia"];
            $projeto->inicio  = $input["inicio"];
            $projeto->fim = $input["fim"];
            $projeto->save();
            
            return redirect('/projeto/todos-projetos');        
        }
            
    protected function remover_projeto(){
        //Atribuindo váriavel item a partir do codigo da url -----------------------------|        
            $id = Route::getCurrentRoute()->parameters()['id'];
            $projeto = Projeto::where('id','=',$id)->first();
        
        return view('projeto.removerProjeto', compact('projeto'));
    }

    protected function remova_projeto(){
        $id = Route::getCurrentRoute()->parameters()['id'];
        $projeto = Projeto::where('id','=',$id)->first();
        
        Projeto::where('id', $projeto->id)->delete();
        
        return redirect('/projeto/todos-projetos');        
    }
}
