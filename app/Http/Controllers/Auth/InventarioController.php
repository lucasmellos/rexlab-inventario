<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CalendarioController as Cal;
use App\Http\Controllers\Controller;
use View;
use Auth;
use DB;
use App\Item;
use App\Categoria;
use App\Projeto;
use App\Local;
use App\Requisicao;
use Request;
use Route;
use File;
use Illuminate\Support\Facades\Input;

class InventarioController extends Controller{
    
    public function mostrar_todos_items(){
        //Atribuindo items toda a tabela e retornando para view ------------------------|
            $items = Item::paginate(10);
            
            $projetos = Projeto::All();
            $categorias = Categoria::All();
            $locais = Local::All();

            $allCount = Item::count();
            $dispCount = Item::where('status','=','Disponivel')->count();
            $empresCount = Item::where('status','!=','Disponivel')->count();
            
        return View::make('inventario.todosItems', compact('items','categorias','locais','projetos','allCount','dispCount','empresCount'));
    }

    public function mostrar_todos_categor(){
        $categors = Categoria::all();

        return View::make('inventario.todosCategor', compact('categors'));
    }
    
    public function criar_item(){
        //Atribuindo variaveis para retornar a view e preencher os select-group's-------|
            $projetos = Projeto::all();
            $categorias = Categoria::all(); 
            $locais = Local::all();
        
        return View('inventario.adicionaItem', compact('projetos','locais','categorias'));
    }

    public function crie_item(){
        $input = Request::all();

        $new['nome'] = $input['nome'];
        $new['projeto'] = $input['projeto'];
        $new['categoria'] = $input['categoria'];
        $new['emprestavel'] = $input['emprestavel'];
        $new['local'] = $input['local'];
        $input['patrimonio'] != NULL ? $new['patrimonio'] = $input['patrimonio'] : $new['patrimonio'] = 0;
        $new['status'] = 'Disponivel';
        
        $projeto_ano = Projeto::where('id',$input['projeto'])->value('inicio');         
        $categoria_item = sprintf("%02d", $input['categoria']);
        $projeto_id = sprintf("%02d", $input['projeto']);
        $base_codigo = $projeto_ano.$projeto_id.$categoria_item;

        $new['codigo'] = Item::where('categoria','=',$input['categoria'])
                        ->where('projeto','=',$input['projeto'])
                        ->orderBy('id', 'desc')
                        ->value('codigo');
        
        $new['codigo'] != null ? $new['codigo'] += 1 : $new['codigo'] = $base_codigo.'001';
        
        //Nota Fiscal -------------------------------------------------------------------|
            $new['nf_format'] = Input::file('notaf')->getClientOriginalExtension();
            $new['nf_size'] = Input::file('notaf')->getSize();
            $tmp_path = Input::file('notaf')->getRealPath();
            $fileName = uniqid() . '.' . $new['nf_format'];
            $path = 'nf/';
            Input::file('notaf')->move(public_path().'/'.$path, $fileName);
            $new['nf_url'] = $path . $fileName;
        //-------------------------------------------------------------------------------|
        
        if($input['quant'] == 1){
            Item::create($new);
        }else{
            for($n = 1; $n <= $input['quant'];$n++){
                if($n != 1){
                    $new['codigo'] += 1;
                }               
                Item::create($new);
            }
        }
        
        $lastPage = ceil(Item::count() / 10);
        
        return redirect('/inventario/todos-items?page='.$lastPage);
    }
    
    public function procurar_item(){
        Cal::mudaDisp();
        Cal::mudaDev();
        //Atribuindo variaveis para retornar a view e preencher os select-group's-------|
            $projetos = Projeto::all();
            $locais = Local::all();
            $categorias = Categoria::all(); 
        //------------------------------------------------------------------------------|
        return view('inventario.busca', compact('projetos','locais','categorias'));
    }
    
    public function procure_item(){
        Cal::mudaDisp();
        Cal::mudaDev();
        
        $input = Request::all();
        $categorias = Categoria::all();
        //Se o campo código for vázio então a busca será feita pelos select's ------------|
        if($input['codigo'] == NULL){
            ($input['projeto'] != 0 ) ? ($projeto = "and `projeto` = ".$input['projeto']) : ($projeto = "");
            ($input['categoria'] != 0) ? ($categoria = "and `categoria` = ".$input['categoria']) : ($categoria = "");
            ($input['local'] != 0) ? ($local = "and `local` = ".$input['local']) : ( $local = "");
 
            $items = DB::select('select * from `items` where 1 '.$projeto." ".$categoria." ".$local."ORDER BY `items`.`status` ASC");
            
        }else{
            $items = Item::where('codigo', $input['codigo'])->orWhere('patrimonio', $input['codigo'])->get();
        }
        return View('inventario.resultado',compact('items','categorias'));
    }
    protected function requisitar_item(){
        Cal::mudaDisp();
        Cal::mudaDev();
        
        $codigo = Route::getCurrentRoute()->parameters()['codigo'];
        $item = Item::where('codigo','=',$codigo)->orWhere('patrimonio',$codigo)->first();
        $quant = Item::where('categoria',$item->categoria)->where('status','Disponivel')->count();
        
        $locais = Local::all();
        $categorias = Categoria::all();
        $projetos = Projeto::All();
        
        return view('inventario.requisitarItem',compact('item','quant','locais','projetos','categorias'));
    }
    
    protected function requisite_item(){
        Cal::mudaDisp();
        Cal::mudaDev();
        
        $input = Request::all();

        $new['codigo'] = Route::getCurrentRoute()->parameters()['codigo'];
        $new['user'] = Auth::User()->id;
        $new['local'] = $input['local'];
        $new['quantidade'] = $input['quant'];
        $new['item'] = Item::where('codigo','=',$new['codigo'])->orWhere('patrimonio',$new['codigo'])->value('nome');
        $new['categoria'] = Item::where('codigo','=', $new['codigo'])->orWhere('patrimonio',$new['codigo'])->value('categoria');


        if($new['quantidade'] != 1){
            for($i=0;$i<$new['quantidade'];$i++){
                $item = Item::where('categoria',$new['categoria'])->where('status','Disponivel')->where("emprestavel",'1')->first();
                $item->status = 'Requisitado';
                $new['codigo'] = $item->codigo;
                $item->save();
        
                Requisicao::create($new);                
            }
                return redirect('/user/minhas-requ');
        }
        else{
            $item = Item::where('codigo','=', $new['codigo'])->orWhere('patrimonio',$new['codigo'])->first();
            if($item->status == 'Disponivel' && $item->emprestavel == 1){
                $item->status = 'Requisitado';
                $item->save();

                Requisicao::create($new);

                return redirect('/user/minhas-requ');
            }
        }
    }
    
    protected function  requisitar_varios(){
        $input = Request::all();
        $items = $input['item'];
        var_dump($items);die;
        return view('inventario.requisitarItems', compact('items'));
    }
    
    protected function devolver_item(){
        Cal::mudaDisp();
        Cal::mudaDev();

        $cdg = Route::getCurrentRoute()->parameters()['codigo'];
        $item = Item::where('codigo', $cdg)->first();
        $requisicao = Requisicao::where('item','=',$item)->first();

        return view('inventario.devolveItem',compact('requisicao','item','count'));
    }

    protected function devolva_item(){
        Cal::mudaDisp();
        Cal::mudaDev();
        
        $codigo = Route::getCurrentRoute()->parameters()['codigo'];
        $item = Item::where('codigo','=', $codigo)->first();
        if($item != NULL){
            if($item->status == 'Requisitado'){
                $item->status = 'Disponivel';
                $item->save();

                Requisicao::where('codigo', $codigo)->delete();
                
                return redirect('/user/minhas-requ');
            }
        }else{
            Requisicao::where('codigo', $codigo)->delete();
                
            return redirect('/user/minhas-requ');
        }
    }
    
    protected function editar_item(){
        //Atribuindo variaveis para retornar a view e preencher os select-group's-------|
            $projetos = Projeto::all();
            $locais = Local::all();
            $categorias = Categoria::all();
            
        //Atribuindo váriavel item a partir do codigo da url ---------------------------|
            $codigo = Route::getCurrentRoute()->parameters()['codigo'];
            $item = Item::where('codigo','=',$codigo)->first();
       
        return view('inventario.editarItem', compact('item','projetos','locais','categorias'));
    }
    
    protected function edite_item(){
        $input = Request::all();
        //Atribuindo váriavel item a partir do codigo da url -----------------------------|        
            $codigo = Route::getCurrentRoute()->parameters()['codigo'];
            $item = Item::where('codigo','=',$codigo)->first();
            
        //Editar atributos do item requer uma atualização do codigo do item -------------|
        //Codigo do Item ----------------------------------------------------------------|            
            $anoProjeto = Projeto::where('id',$input['projeto'])->value('inicio');
            $idProjeto = $input['projeto'];
            $tipoItem = $input['categoria'];
            //Conversão para o padrão de casas decimais----------------------------------|
                $tipoItem = sprintf("%02d", $tipoItem);
                $itemId = sprintf("%03d", $item['cod']);
                $idPojeto = sprintf("%02d", $idProjeto);
            //---------------------------------------------------------------------------|
            $new['codigo'] = $anoProjeto.$idPojeto.$tipoItem.$itemId;
        //-------------------------------------------------------------------------------|
        //Atualizando valores no banco --------------------------------------------------|    
            $item->nome = $input["nome"];
            $item->projeto  = $input["projeto"];
            $item->categoria  = $input["categoria"];
            $item->local = $input["local"];
            $item->cod = $item['cod'];
            $item->codigo = $new['codigo'];
            $item->patrimonio = $input['patrimonio'];
            $item->save();
       
        return redirect('/inventario/todos-items');
    }
    
    protected function remover_item(){
            $categorias = Categoria::all();
            $locais = Local::all();
            $projetos = Projeto::all();

        //Atribuindo váriavel item a partir do codigo da url -----------------------------|        
            $codigo = Route::getCurrentRoute()->parameters()['codigo'];
            $item = Item::where('codigo','=',$codigo)->first();
            
        return view('inventario.removeItem', compact('item','categorias','projetos','locais'));
    }
    
    protected function remova_item(){
        //Atribuindo váriavel item a partir do codigo da url -----------------------------|    
            $codigo = Route::getCurrentRoute()->parameters()['codigo'];
            $item = Item::where('codigo','=',$codigo)->first();
        
        //Deletar no banco ---------------------------------------------------------------|
            Item::where('codigo', $item->codigo)->delete();
            
        //Checar se outro registro usa a mesma NF ----------------------------------------|    
            $count = Item::where('nf_url', $item->nf_url)->count();
            
        //Deletar na pasta public --------------------------------------------------------|
            if($count == 0){
                File::delete(public_path() ."/". $item->nf_url);
            }
        return redirect('inventario/todos-items');        
    }
}
