<?php

function admin(){
        $input = Input::all();
        $users = explode(',',$input['users']);
        
        foreach($users as $user){
            User::where('username', $user)
                    ->where('type', '<>', 'admin')
                    ->update(['type'=>'admin']);
        }
    }