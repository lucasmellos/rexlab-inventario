<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model{
    public $table = "items";
    
    protected $fillable = [
        'categoria', 
        'codigo',
        'emprestavel', 
        'local', 
        'nome', 
        'status',
        'notaf', 
        'projeto', 
        'nf_size',
        'nf_url',
        'nf_format',
        'patrimonio'
    ];

}
