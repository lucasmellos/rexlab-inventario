<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendario extends Model
{
    protected $table = 'calendarios';
    
    protected $fillable = [
      'name',
      'criador',
      'startdate',
      'enddate',
      'starttime',
      'endtime',
      'color',
      'local',
      'materiais',
      'quantidade' 
    ];
}