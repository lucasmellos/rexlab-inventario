@extends('layout.base')

@section ('title')
    <title>Controle | Categorias</title>
@stop

@section ('head')
<style>
    td, th{
        text-align: center;
    }

    tr:nth-child(even) {background: #EEE}
    tr:nth-child(odd) {background: #FFF}

</style>
@stop

@section ('content')
<h1 class="subtitle">Categorias Cadastradas</h1>
    <div style="overflow-x:auto;">    
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th></th>
            </tr>
            @foreach($categorias as $categoria)
            <tr>
                <td>{{sprintf("%02d",$categoria->id)}}</td>
                <td>{{$categoria->nome}}</td>
                <td style="text-align: right;">
                    <a href="/categor/{{$categoria->id}}/remover-categor" data-toggle="tooltip" title="Remover" class="glyphicon glyphicon-trash"></a> 
                    <a href="/categor/{{$categoria->id}}/editar-categor" data-toggle="tooltip" title="Editar" class="glyphicon glyphicon-pencil"></a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
@stop

@section ('script')
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
@stop