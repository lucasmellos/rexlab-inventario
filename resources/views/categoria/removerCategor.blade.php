@extends('layout.base')

@section ('title')
    <title>Controle | Remover Categoria</title>
@stop

@section ('head')
<style>
    #bot{
        position: fixed;
        bottom: 10px;
        padding: 5px 10px;
    }
    #infor{
        margin-top: 25px;
    }

</style>
@stop

@section ('content')
<h1 class="subtitle">Remover Categoria</h1>
<form method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-lg-12 col-md-9 col-sm-6" id="infor">
        <ul class="list-group panel-default" style="text-align: center">
            <li class="list-group-item">Categoria: {{$categoria->nome}}</li>
            <li class="list-group-item">Id: {{$categoria->id}}</li>
        </ul>
    </div>
    <div id="bot">
        <button type="submit" class="btn btn-default">Remover</button>
        <a class="btn btn-primary" onclick="window.history.back()">Cancelar</a>
    </div>
</form>
@stop
