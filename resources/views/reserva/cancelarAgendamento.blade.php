@extends('layout.base')

@section ('title')
    <title>Controle | Cancelar Agendamento</title>
@stop

@section ('head')
<style>
    #bot{
        position: fixed;
        bottom: 10px;
        padding: 5px 10px;
    }
    #infor{
        margin-top: 25px;
    }

</style>
@stop

@section ('content')
<h1 class="subtitle">Cancelar Agendamento</h1>
<form method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-lg-12 col-md-9 col-sm-6" id="infor">
        <ul class="list-group panel-default" style="text-align: center">
            <li class="list-group-item">Início: {{$dataR->retirada}}</li>
            <li class="list-group-item">Fim: {{$dataR->devolucao}}</li>
            <li class="list-group-item">Hora de início: {{$dataR->hretirada}}</li>
            <li class="list-group-item">Hora de término: {{$dataR->hdevolucao}}</li>
            <li class="list-group-item">Local: {{$dataL->nome}}</li>
        </ul>
    </div>
    <div id="bot">
        <button type="submit" class="btn btn-default">Remover</button>
        <button class="btn btn-default" onclick="window.history.back()">Cancelar</button>
    </div>
</form>

@stop