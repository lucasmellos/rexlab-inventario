@extends ('layout.baseLogin')
@section ('head')
	<title>Controle | 404</title>
    <style>
        section{
            text-align: center;
        }
        h1{
            margin-top: 10%;
            font-size: 150px;
        }
        span{
            font-size: 20px;
        }
    </style>
@stop

@section ('content')
<div>
    <section>
      <h1>404 <span>Pagina não encontrada!</span></h1>
      <p style="font-weight: 700;">Por algum motivo a página solicitada não pôde ser encontrada no servidor :/</p>
      <p style="margin-top: 15px;"><a href="javascript:history.go(-1)">&laquo; Voltar</a> / <a href="/home">Página inicial &raquo;</a></p>
    </section>
</div>
@stop