@extends('layout.base')

@section ('title')
    <title>Controle | Painel de Controle</title>
@stop

@section ('head')
<style>
    #painel{
        margin-top: 25px;
    }
    #bot{
        position: fixed;
        bottom: 10px;
        padding: 5px 10px;
    }
</style>
@stop

@section ('content')
<h1 class="subtitle">Informações pessoais</h1>
<div id="painel" class="col-lg-8">
    <ul class="list-group panel-default">
        <li class="list-group-item"><i class="fa fa-user">&nbsp;&nbsp;</i>Nome: {{$data->name}} {{$data->lastname}}</li>
        <li class="list-group-item"><i class="fa fa-star">&nbsp;&nbsp;</i>Tipo: <b>{{$data->userType}}</b></li>        
        <li class="list-group-item"><i class="fa fa-envelope-o">&nbsp;&nbsp;</i>E-mail: {{$data->email}}</li>
        <li class="list-group-item"><i class="fa fa-flag">&nbsp;&nbsp;</i>Cadastrado em: <b>{{$data->created_at}}</b></li>
    </ul>

    <div id="bot">
        <a class="btn btn-default" href="/user/epainel">Editar</a>
        <a class="btn btn-primary" href="/home">Início</a>
    </div>
</div>
@stop