@extends('layout.base')

@section ('title')
    <title>Controle | Requisições</title>
@stop

@section ('head')
<style>
    td, th{
        text-align: center;
    }
    center{
        margin-top: 10%;
        padding: 30px;
        font-size: 20px;
    }
    #bot{
        position: fixed;
        bottom: 10px;
        right: 0px;
        padding: 0px 50px 10px 0px;
    }
    tr:nth-child(even) {background: #EEE}
    tr:nth-child(odd) {background: #FFF}
</style>
@stop

@section ('content')
<h1 class="subtitle">Minhas Requisições</h1>
@if(sizeof($requisicoes) != 0)
<div style="overflow-x:auto;">    
    <table class="table">
        <tr>
            <th><i class="fa fa-bookmark-o"></i></th>
            <th>Item</th>
            <th>Código</th>
            <th>Local</th>
            <th>Data da Requisição</th>
            <th></th>
        </tr>

        @foreach($requisicoes as $requisicao)
        <tr>
            <td>{{$loop->index+1}}</td>
            <td>{{$requisicao->item}}</td>
            <td>{{$requisicao->codigo}}</td>
            <td>{{$locais[$requisicao->local-1]->nome}}</td>
            <td>{{$requisicao->created_at}}</td>
            <td>
                <a href="/inventario/devolver-item/{{$requisicao->codigo}}" data-toggle="tooltip" title="Devolver"><i class="fa fa-mail-reply-all"></i></a>
            </td>
        </tr>
        @endforeach
    </table>
    <div id="bot">
        <a href="/user/devolver-tudo" class="btn btn-default">Devolver Tudo <span class="fa fa-magic"></span></a>
    </div>
    @else
        <center>Nenhuma requisição!</center>
    @endif
</div>
@stop

@section ('script')
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
@stop
