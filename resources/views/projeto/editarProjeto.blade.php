@extends('layout.base')

@section ('title')
    <title>Controle | Editar Projeto</title>
@stop

@section ('head')
<style>
    #bot{
        position: fixed;
        bottom: 10px;
        padding: 5px 10px;
    }
</style>
@stop

@section ('content')
<h1 class="subtitle col-lg-8"><i class="fa fa-tags">&nbsp;</i>Editar Projeto</h1>
<form method="POST" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-lg-8">
        <label style="padding-bottom: 5px">Nome</label>
        <input class="form-group form-control" value="{{$projeto->nome}}" name="nome" required>            
        <label style="padding-bottom: 5px">Agência</label>
        <input class="form-group form-control" value="{{$projeto->agencia}}" name="agencia" required>            
        <label style="padding-bottom: 5px">Ano inicial</label>
        <input class="form-group form-control" value="{{$projeto->inicio}}" name="inicio" required>            
        <label style="padding-bottom: 5px">Ano final</label>
        <input class="form-group form-control" value="{{$projeto->fim}}" name="fim" required>            

        <div id="bot">
            <button type="submit" class="btn btn-default">Editar</button>
            <a class="btn btn-primary" onclick="window.history.back()">Cancelar</a>
        </div>
    </div>
</form>
@stop
