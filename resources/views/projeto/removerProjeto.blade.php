@extends('layout.base')

@section ('title')
    <title>Controle | Remover Projeto</title>
@stop

@section ('head')
<style>
    #bot{
        position: fixed;
        bottom: 10px;
        padding: 5px 10px;
    }
    #infor{
        margin-top: 25px;
    }

</style>
@stop

@section ('content')
<h1 class="subtitle col-lg-8"><i class="fa fa-tags">&nbsp;</i>Remover Projeto</h1>
<form method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-lg-8 col-md-9 col-sm-6" id="infor">
        <ul class="list-group panel-default" style="text-align: center">
            <li class="list-group-item">Projeto: {{$projeto->nome}}</li>
            <li class="list-group-item">Agência: <b>{{$projeto->agencia}}</b></li>
        </ul>
    </div>
    <div id="bot">
        <button type="submit" class="btn btn-default">Remover</button>
        <button class="btn btn-primary" onclick="window.history.back()">Cancelar</button>
    </div>
</form>
@stop
