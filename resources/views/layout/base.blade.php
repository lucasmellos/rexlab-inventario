<!doctype html>
<html lang="pt" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/monthly.css')}}">
    <link rel="stylesheet" href="{{asset('css/reset.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css'>
    @yield('title')
    <link rel="icon" type="image/png" href="http://150.162.233.21/img/hello.png"/>
    <style>.subtitle,.title{margin-top:25px;margin-bottom:10px;font-size:30px}.title{text-align:center}.subtitle{text-align:left;padding-bottom:5px;border-bottom:1px solid #ccc}.point{cursor:pointer}.point:hover{text-decoration:none}.espacim{width:102px}</style>
    @yield('head')
</head>
<body>
    <header class="cd-main-header">
            <a href="/home" class="cd-logo"><img src="{{asset('img/logo.png')}}" style="width: 100px;" alt="Logo"></a>
            <a class="cd-nav-trigger"><span></span></a>
            <nav class="cd-nav">
                    <ul class="cd-top-nav">
                            <li class="has-children account">
                                <a class="point">
                                    <div class="espacim"></div><img src="{{asset('img/cd-avatar.png')}}" alt="avatar">
                                        Olá {{Auth::user()->name}}!
                                </a>                                       
                                <ul>    
                                    <li><a class="point" href="/user/painel">Minha Conta</a></li>
                                    <li><a class="point" href="http://www.rexlab.ufsc.br">Site RExLab</a></li>
                                    <li><a class="point" href="/logout">Sair</a></li>
                                </ul>
                            </li>
                    </ul>
            </nav>
    </header>

    <main class="cd-main-content">
        <nav class="cd-side-nav">
            <ul>
                <li class="cd-label">Menu Principal</li>
                <li class="has-children users">
                        <a class="point">Pendências</a>
                        <ul>
                                <li><a class="point" href="/agendamento/agenda-{{Auth::User()->name}}">Minhas Reservas</a></li>						
                                <li><a class="point" href="/user/minhas-requ">Minhas Requisições</a></li>
                        </ul>
                </li>
                <li class="has-children bookmarks">
                    <a class="point">Reserva</a>
                    <ul>
                        <li><a class="point" href="/calendario">Calendário</a></li>
                        <li><a class="point" href="/agendamento/agenda">Todas as Reservas</a></li>
                    </ul>
                </li>
            @if(Auth::user()->userType === 'A')
                <li class="has-children overview">
                    <a class="point">Inventário</a>
                    <ul>
                        <li><a class="point" href="/inventario/adicionar-item">Cadastrar Item</a></li>
                        <li><a class="point" href="/inventario/buscar-item">Buscar Item</a></li>
                        <li><a class="point" href="/inventario/todos-items">Todo Inventário</a></li>
                    </ul>
                </li>
            <li class="has-children bookmarks">
                <a class="point">Controle base</a>
                <ul>
                    <li><a class="point" href="/projeto/adicionar-projeto">Cadastrar Projeto</a></li>
                    <li><a class="point" href="/local/adicionar-local">Cadastrar Local</a></li>
                    <li><a class="point" href="/categor/adicionar-categor">Cadastrar Categoria</a></li>
                    <li><a class="point" href="/projeto/todos-projetos">Listar Projetos</a></li>                                    
                    <li><a class="point" href="/local/todos-locais">Listar Locais</a></li>  
                    <li><a class="point" href="/categor/todos-categor">Listar Categorias</a></li>
                </ul>
            </li>
            @endif
            </ul>
        </nav>
        <div class="content-wrapper">
        @yield('content') 
        </div>
    </main>        
    <script src="{{asset('js/jquery-2.1.4.js')}}"></script>
    <script src="{{asset('js/jquery.menu-aim.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/modernizr.js')}}"></script>
    @yield('script')
</body>
</html>