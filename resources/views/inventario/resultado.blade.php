@extends('layout.base')

@section ('title')
    <title>Controle | Resultados</title>
@stop

@section ('head')
<style>
    .td, .th{
        text-align: center;
    }
    .center{
        margin-top: 10%;
        padding: 30px;
        font-size: 20px;
    }
    #bot{
        position: fixed;
        bottom: 10px;
        right: 0px;
        padding: 0px 50px 10px 0px;
    }    
    .bg{
        background-color:#e6ecff !important;
    }
</style>
@stop

@section ('content')
<h1 class="subtitle">Resultados</h1>

@if(sizeof($items) != 0)
<div style="overflow-x:auto;"> 
    <form method="POST" enctype="multipart/form-data" action="/inventario/requisitar-items" autocomplete="off">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <table class="table table-hover table-reponsive table-striped">
        <tr>
            <th></th>
            <th>Código</th>
            <th>Nome</th>
            <th>Categoria</th>
            <th>Status</th>
            <th></th>
            <th></th>
        </tr>

        @foreach($items as $item)
        <tr class="focus">
            <td style="text-align: right">
                @if($item->emprestavel == 1 && $item->status == 'Disponivel')
                <input class="oi_sumido" type="checkbox" value="{{$item->codigo}}" name="item[]"/>
                @endif
            </td>
            <td>{{$item->codigo}}</td>
            <td>{{$item->nome}}</td>
            <td>{{$categorias[$item->categoria-1]->nome}}</td>
            @if($item->status == "Disponivel")
            <td style="color: green; font-weight: 580">{{$item->status}}</td>
            @else
            <td style="color: red; font-weight: 580">{{$item->status}}</td>
            @endif                
            <td></td>
            <td style="text-align: right">
                <a href="/inventario/{{$item->codigo}}/remover-item" data-toggle="tooltip" title="Remover" class="glyphicon glyphicon-trash"></a> 
                <a href="/inventario/{{$item->codigo}}/editar-item" class="glyphicon glyphicon-pencil"></a>
            </td>
        </tr>
        @endforeach
    </table>
    
    <div style="text-align: right">
        <button class="btn btn-default" type="submit" disabled>Solicitar</button>
    </div>
    @else
        <center>Nenhum item encontrado!</center>
    @endif
    </form>
</div>
@stop

@section ('script')
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
   
        $('table.table-striped tr').click(function(event) {
            if (event.target.type !== 'checkbox') {
                $(':checkbox', this).trigger('click');
            }
        });
    });
</script>
@stop