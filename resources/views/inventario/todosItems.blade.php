@extends('layout.base')

@section ('title')
    <title>Controle | Inventário</title>
@stop

@section ('head')
<style>
    td, th{
        text-align: center;
    }
    center{
        margin-top: 10%;
        padding: 30px;
        font-size: 20px;   
    }
    .dados{
        padding-bottom: 5px;
    }
    #header{
        margin-bottom: -10px;
    }

    tr:nth-child(even) {background: #EEE}
    tr:nth-child(odd) {background: #FFF}

</style>
@stop

@section ('content')
<h1 class="subtitle"><i class="fa fa-inbox">&nbsp;</i>Inventário completo</h1>
<div id='header'>
    <ul class="list-group panel-default">
      <li class="list-group-item col-lg-12" style="text-align: center">
          <label class="dados" style="padding-right: 2cm;">Nº Itens cadastrados: <span style="font-weight: 900;">{{$allCount}}&nbsp;</span></label>
          <label class="dados" style="padding-right: 2cm;">Nº Itens disponiveis: <span style="font-weight: 900;">{{$dispCount}}&nbsp;&nbsp;&nbsp;</span></label>
          <label style="padding-right: 2cm;">Nº Itens emprestados: <span style="font-weight: 900;">{{$empresCount}}</span></label>
      </li>
    </ul>
</div>
@if(sizeof($items) != 0)
    <div style="overflow-x:auto;">    
        <table class="table">
            <tr>
                <th>NF</th>
                <th></th>
                <th>Código</th>
                <th>Nome</th>
                <th>Categoria</th>
                <th></th>
            </tr>

            @foreach($items as $item)
            <tr>
                <td><a target="_blank" href="{{asset($item->nf_url)}}" class="glyphicon glyphicon-file"></a><td>
                <td>{{$item->codigo}}</td>
                <td>{{$item->nome}}</td>
                <td>{{$item->categoria}}</td>
                <td style="text-align: right">
                    <a href="{{$item->codigo}}/remover-item" data-toggle="tooltip" title="Remover" class="glyphicon glyphicon-trash"></a> 
                    <a href="{{$item->codigo}}/editar-item" data-toggle="tooltip" title="Editar" class="glyphicon glyphicon-pencil"></a>
                    @if($item->emprestavel == 1 && $item->status == 'Disponivel')
                    <a href="/inventario/requisitar-item/{{$item->codigo}}" data-toggle="tooltip" title="Requisitar" class="glyphicon glyphicon-time"></a>
                    @endif
                    </td>
            </tr>
            @endforeach
        </table>
    </div>
    {{$items->links()}}

    <div style='text-align: right'>
        <button href="#" class="btn btn-default">Gerar PDF <span class="glyphicon glyphicon-file"></span></button>
    </div>
@else
    <center>Nenhum item encontrado!</center>
@endif
@stop

@section ('script')
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
@stop
