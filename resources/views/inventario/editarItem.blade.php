@extends('layout.base')

@section ('title')
    <title>Controle | Editar Item</title>
@stop

@section ('head')
<style>
    #bot{
        position: fixed;
        bottom: 10px;
        padding: 5px 10px;
    }
</style>
@stop

@section ('content')
<h1 class="subtitle col-lg-8">Editar Item - {{$item->nome}}</h1>
<form method="POST" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-lg-8">
        <label style="margin-bottom: 5px">Nome</label>
        <input class="form-group form-control" value="{{$item->nome}}" name="nome" required>

        <label style="margin-bottom: 5px">  Código de Patrimonio &nbsp;<small style="text-align: right">(Se não existir, preencher com '0')</small></label>
        <input type="number" class="form-group form-control" value="{{$item->patrimonio}}" name="patrimonio" required>            

        <label style="margin-bottom: 5px">Projeto</label>
        <select class="form-group form-control" id="proje_id" name="projeto" required>
            @foreach($projetos as $projeto)
            <option value="{{$projeto->id}}">{{$projeto->nome}}</option>
            @endforeach
        </select>

        <label style="margin-bottom: 5px">Categoria</label>
        <select class="form-group form-control" name="categoria" id="categ_id" required>
            @foreach($categorias as $categoria)
            <option value="{{$categoria->id}}">{{$categoria->nome}}</option>
            @endforeach
        </select>

        <label style="margin-bottom: 5px">Local</label>
        <select class="form-group form-control" id="local_id" name="local" required>
            @foreach($locais as $local)
            <option value="{{$local->id}}">{{$local->nome}}</option>
            @endforeach
        </select>
    </div>
    <div id="bot">
        <button type="submit" class="btn btn-default">Editar</button>
        <a class="btn btn-primary" onclick="window.history.back()">Cancelar</a>
    </div>
</form>
@stop

@section ('script')
<script>
    $(function() {
        $('#proje_id').val({{$item->projeto}});
        $('#categ_id').val({{$item->categoria}});
        $('#local_id').val({{$item->local}});
    });
</script>
@stop