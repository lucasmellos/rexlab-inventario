@extends('layout.base')

@section ('title')
    <title>Controle | Devolver Item</title>
@stop

@section ('head')
<style>
    #bot{
        position: fixed;
        bottom: 10px;
        padding: 5px 10px;
    }
    #infor{
        margin-top: 25px;
    }
    #disp{
        margin-top: -22px;
    }
    #disp::-webkit-input-placeholder{
        text-align: center;
    }

</style>
@stop

@section ('content')
<h1 class="subtitle col-lg-10">Devolver Item</h1>
<form method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-lg-10">
        <div class="col-lg-12 col-md-9 col-sm-6" id="infor">
            <ul class="list-group panel-default" style="text-align: center">
                <li class="list-group-item">Item: {{$item->nome}}</li>
                <li class="list-group-item">Código: <b>{{$item->codigo}}</b></li>
            </ul>
        </div>
        <div id="bot">
            <button type="submit" class="btn btn-default">Devolver</button>
            <button class="btn btn-primary" onclick="window.history.back()">Cancelar</button>
        </div>
    </div>
</form>
@stop
