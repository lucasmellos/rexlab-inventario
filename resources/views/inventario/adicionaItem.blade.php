@extends('layout.base')

@section ('title')
    <title>Controle | Cadastrar Item</title>
@stop

@section ('head')
<style>
    select{
        margin-bottom: 5px;
        width: 100%;
    }
    select:required:invalid {
        color: #999999;
    }
    option[value=""][disabled] {
        display: none;
    }
    option {
        color: black;
    }
    #bot{
        position: absolute;
        bottom: 10px;
        padding: 5px 10px;
    }
    .arquivo {
        overflow: hidden;
        position: relative;
        cursor: pointer;
    }
    .arquivo [type=file] {
        cursor: inherit;
        display: block;
        filter: alpha(opacity=0);
        min-height: 100%;
        min-width: 100%;
        opacity: 0;
        position: absolute;
        right: 0;
        text-align: right;
        top: 0;
    }
    #cdg::-webkit-input-placeholder{
        text-align: center;
    }
</style>
@stop

@section ('content')
<form class="form-horizontal" method="POST" action="/inventario/adicionar-item" autocomplete="off" enctype="multipart/form-data">
    {{ csrf_field() }}
    <h1 class="subtitle  col-lg-8">Cadastrar Item</h1>
    <div class="col-lg-8 col-md-6 col-sm-4" style="margin: 0 auto;">
        <input class="form-group form-control" placeholder="Nome" name="nome">
        <input id='cdg' class="form-group form-control" placeholder="Código (UFSC - Patrimônio)" name="patrimonio" id="pat">
        <label class="arquivo form-group form-control" style="text-align: center;">
            <span class="glyphicon glyphicon-upload	Try it"></span> &nbsp 
            <input placeholder="Nota Fiscal" name="notaf" type="file" required>
            <label>Inserir Nota Fiscal</label>
        </label>
        <select class="form-group form-control" name="projeto" required>
            @foreach($projetos as $projeto)
            <option value="{{$projeto->id}}">{{$projeto->nome}}</option>
            @endforeach
        </select>
        <select class="form-group form-control" name="categoria" required>
            @foreach($categorias as $categoria)
            <option value="{{$categoria->id}}">{{$categoria->nome}}</option>
            @endforeach
        </select>
        <input class="form-group form-control" type="number" placeholder="Quantidade" name="quant" id="quant" value="1">            
        <select class="form-group form-control" name="local" required>
            @foreach($locais as $local)
            <option value="{{$local->id}}">{{$local->nome}}</option>
            @endforeach
        </select>
        Pode ser emprestado? &nbsp 
        <label>
            <input type="radio" name="emprestavel" value="1" checked> Sim
            <input type="radio" name="emprestavel" value="0"> Não
        </label>
    </div>
    <div id="bot">
        <button type="submit" class="btn btn-default">Adicionar</button>
        <a class="btn btn-primary" onclick="window.history.back()">Cancelar</a>
    </div>
</form>        
@stop

@section ('script') 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
    $("[type=file]").on("change", function(){
      // Name of file and placeholder
      var file = this.files[0].name;
      var dflt = $(this).attr("placeholder");
      if($(this).val()!=""){
        $(this).next().text(file);
      } else {
        $(this).next().text(dflt);
      }
    });
    
    var dis1 = document.getElementById("cdg");
    dis1.onchange = function () {
        if (this.value != "" || this.value.length > 0) {
            document.getElementById("quant").disabled = true;
            document.getElementById("quant").value = 1;
        }else{             
            document.getElementById("quant").disabled = false;
        }
    }
</script>  
@stop