<?php
Auth::routes();

Route::middleware(['auth'])->group(function(){
    Route::get('/home','HomeController@index');
    Route::get('/','HomeController@index');
    
    Route::get('/calendario','CalendarioController@agendar');
    Route::post('/calendario','CalendarioController@agende');
    
    Route::post('/calendario-disp','\App\Http\Requests\DataUsada@rules');
    Route::get('/calendario-quant','\App\Http\Requests\Quant@rules');
    Route::get('/calendario-MatRes','\App\Http\Requests\MatRes@rules');

    Route::get('/logout', 'Auth\LogoutController@logout');
    
    Route::group(array('prefix' => 'agendamento'), function() {
        Route::get('/agenda','CalendarioController@todos');
        Route::get('/agenda-{name}','CalendarioController@agendaUsuario');
        
        Route::get('/cancelar-agendamento-{id}','CalendarioController@cancelarAgendamento');
        Route::post('/cancelar-agendamento-{id}','CalendarioController@canceleAgendamento');
    });

    Route::group(array('prefix' => 'inventario'), function() {
        Route::get('/adicionar-item','InventarioController@criar_item');
        Route::post('/adicionar-item','InventarioController@crie_item');
        
        Route::get('/buscar-item','InventarioController@procurar_item');
        Route::post('/resultado-item','InventarioController@procure_item');
        
        Route::get('/requisitar-item/{codigo}/','InventarioController@requisitar_item');
        Route::post('/requisitar-item/{codigo}/','InventarioController@requisite_item');
        
        Route::get('/devolver-item/{codigo}','InventarioController@devolver_item');
        Route::post('/devolver-item/{codigo}','InventarioController@devolva_item');
        
        Route::get('/{codigo}/editar-item','InventarioController@editar_item');
        Route::post('/{codigo}/editar-item','InventarioController@edite_item');
        
        Route::get('/{codigo}/remover-item', 'InventarioController@remover_item');
        Route::post('/{codigo}/remover-item','InventarioController@remova_item');
        
        Route::post('/requisitar-items','InventarioController@requisitar_varios');
        
        Route::get('/todos-items','InventarioController@mostrar_todos_items');
    });

    Route::group(array('prefix' => 'projeto'), function() {    
        Route::get('/adicionar-projeto','ProjetoController@criar_projeto');
        Route::post('/adicionar-projeto','ProjetoController@crie_projeto');
        
        Route::get('/{id}/editar-projeto','ProjetoController@editar_projeto');
        Route::post('/{id}/editar-projeto','ProjetoController@edite_projeto');
        
        Route::get('/{id}/remover-projeto', 'ProjetoController@remover_projeto');
        Route::post('/{id}/remover-projeto','ProjetoController@remova_projeto');
        
        Route::get('/todos-projetos','ProjetoController@mostrar_todos');
    });

    Route::group(array('prefix' => 'categor'), function() {    
        Route::get('/adicionar-categor','CategoriaController@criar_categoria');
        Route::post('/adicionar-categor','CategoriaController@crie_categoria');
        
        Route::get('/{id}/editar-categor','CategoriaController@editar_categoria');
        Route::post('/{id}/editar-categor','CategoriaController@edite_categoria');
        
        Route::get('/{id}/remover-categor', 'CategoriaController@remover_categoria');
        Route::post('/{id}/remover-categor','CategoriaController@remova_categoria');
        
        Route::get('/todos-categor','CategoriaController@mostrar_todos');
    });

    Route::group(array('prefix' => 'local'), function() {    
        Route::get('/adicionar-local','LocalController@criar_local');
        Route::post('/adicionar-local','LocalController@crie_local');
        
        Route::get('/{id}/editar-local','LocalController@editar_local');
        Route::post('/{id}/editar-local','LocalController@edite_local');
        
        Route::get('/{id}/remover-local', 'LocalController@remover_local');
        Route::post('/{id}/remover-local','LocalController@remova_local');
    
        Route::get('/todos-locais','LocalController@mostrar_todos');
    });

    Route::group(array('prefix' => 'user'), function() {
        Route::get('/painel','UserController@show_user');
        Route::get('/epainel','UserController@editar_user');
        Route::post('/epainel','UserController@edite_user');
    
        Route::get('/minhas-requ','UserController@minhas_requ');
        
        Route::get('/devolver-tudo','UserController@devolver_tudo');
        Route::post('/devolver-tudo','UserController@devolva_tudo');
        
        Route::get('/users','UserController@controlUsers');
    });
});
